package UI 
{
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class ImageButton extends UIElement 
	{
		private var bg_image:BitmapData;
		private var bg_image_over:BitmapData;
		private var text:TextField;		
 
		public var hovered:Boolean;
		public var clicked:Boolean;
		public var func:Function;
 
		public function ImageButton(x:Number = 0, y:Number = 0, bg_image:BitmapData=null, bg_image_over:BitmapData = null, func:Function=null,text_str:String = "", format:TextFormat = null):void
		{
			super(x, y, bg_image.width, bg_image.height);
 
			this.bg_image = bg_image;
			this.bg_image_over = bg_image_over;
 
			text = new TextField();
			if (format == null) {
				format = new TextFormat("myFont", 20, 0x000000, true);
				format.align = "center";
			}
			
			text.width = width;
			text.height = height;
			text.embedFonts = true;
			text.defaultTextFormat = format;
			text.x = x;
			text.y = y;
			text.text = text_str;			
			
			hovered = false;
			clicked = false;
			this.func = func;
		}
		override public function render(Renderer:BitmapData):void 
		{
			if (!visible)
				return;
			if(!hovered||bg_image_over==null){
				Renderer.copyPixels(bg_image, new Rectangle(0, 0, width, height), new Point(x, y));
				Renderer.draw(text, new Matrix(1, 0, 0, 1, x, y));
			}
			else{
				Renderer.copyPixels(bg_image_over, new Rectangle(0, 0, width, height), new Point(x, y));
				Renderer.draw(text, new Matrix(1, 0, 0, 1, x, y));
			}
		}
		override public function update():void 
		{
			if (clicked)
			{
				if (func != null)
					func();
				clicked = false;
			}
			hovered = false;
		}
 
	}
 
}