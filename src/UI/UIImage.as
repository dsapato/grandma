package UI 
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class UIImage extends UIElement {
 
		public function UIImage(x:Number = 0, y:Number = 0, image:BitmapData=null):void{
			super(x, y, image.width, image.height);
			this.image = image;
		}
		
		override public function render(Renderer:BitmapData):void {
			if (!visible) return;
			Renderer.copyPixels(image, new Rectangle(0, 0, width, height), new Point(x, y));
		}
	}
}