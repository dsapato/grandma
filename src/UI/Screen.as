package UI 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.TextEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Screen extends UIElement {
		
		private const MOUSEMIDDLE:int = 10;
		
		public var texts:Array;
		public var buttons:Array;
		public var images:Array;
		
		private var imageBackground:Boolean;
		
		public function Screen(x:Number = 0, y:Number = 0, width:int = 10, height:int = 10, background:Boolean = true, bg_color:uint = 0x888888, imageBackground:Boolean = false, image:BitmapData = null) {
			super(x, y, width, height, background, bg_color);
			texts = new Array();
			buttons = new Array();
			images = new Array();
			this.imageBackground = imageBackground;
			if (image != null)
				this.image = image;
		}
		
		public function AddText(x:Number, y:Number, width:int, height:int, text_str:String, format:TextFormat = null, color:uint = 0xFFFFFF, font:String = "Arial", size:int = 20, background:Boolean = false, bg_color:uint = 0x888888):void {
			var text:TextField = new TextField();
			if (format == null)
				format = new TextFormat("myFont", 20, 0x000000, true);
				
			text.width = width;
			text.height = height;
			text.embedFonts = true;
			text.defaultTextFormat = format;
			text.x = x;
			text.y = y;
			text.text = text_str;
			texts.push(text);
		}	
		
		public function AddTextButton(x:Number, y:Number, width:int, height:int, text:String, format:TextFormat = null, color:uint = 0xFFFFFF, font:String = "Arial", size:int = 20, background:Boolean = false, bg_color:uint = 0x888888, bg_color2:uint = 0x333333, func:Function=null):void{
			buttons.push(new TextButton(x, y, width, height, text, format, background, bg_color, bg_color2, func));
		}
		
		public function addImage(x:Number, y:Number, image:BitmapData):void {
			images.push(new UIImage(x, y, image));
		}
		
		public function addImageButton(x:Number = 0, y:Number = 0, bg_image:BitmapData=null, bg_image_over:BitmapData = null, func:Function=null,text:String = "", format:TextFormat = null):void {
			buttons.push(new ImageButton(x, y, bg_image, bg_image_over, func, text, format));
		}
		
		override public function update():void {
			for (var i:int = 0; i < buttons.length; i++) {
				buttons[i].update();
				if (Game.mousePos.x + MOUSEMIDDLE >= buttons[i].x && Game.mousePos.x + MOUSEMIDDLE <= buttons[i].x + buttons[i].width) {
					if (Game.mousePos.y + MOUSEMIDDLE >= buttons[i].y && Game.mousePos.y + MOUSEMIDDLE <= buttons[i].y + buttons[i].height) {
						buttons[i].hovered = true;
						if (Game.mouse_click)
							buttons[i].clicked = true;
					}
				}
			}
		}
		
		override public function render(Renderer:BitmapData):void {
			if (!visible) return;
			if (background) 
				image.fillRect(new Rectangle(0, 0, width, height), bg_color);
				
			else if (imageBackground)
				image.copyPixels(image, new Rectangle(x, y, width, height), new Point(x, y));
 
			Renderer.copyPixels(image, new Rectangle(0, 0, width, height), new Point(x, y));	
				
			for (var k:int = 0; k < images.length; k++ )
				images[k].render(Renderer);
			
			for (var j:int = 0; j < buttons.length; j++)
				buttons[j].render(Renderer);
				
			for (var i:int = 0; i < texts.length; i++)
				Renderer.draw(texts[i], new Matrix(1, 0, 0, 1, texts[i].x + x , texts[i].y  + y));				
			
		}
	}

}