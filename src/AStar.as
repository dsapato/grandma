package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class AStar {
		//[Embed(source="Resources/Images/bullet.png")]
		//private static var pathTex:Class;
		//private static var bulletSprite:Sprite;
		
		private static const TILESIZE:int = 40;
		private static var map:Array;
		
		public static function search(startX:int, startY:int, goalX:int, goalY:int):Array {
			map = Game.map;
			
			for (var i:int = 0; i < map.length; i ++) {
				for (var j:int = 0; j < map[0].length; j ++ ) {
					map[i][j].visited = false;
					map[i][j].closed = false;
				}
			}
			var open:Array = new Array();
			var start:MapTile = new MapTile(startX / TILESIZE, startY / TILESIZE, true);
			var goal:MapTile = new MapTile(goalX / TILESIZE, goalY / TILESIZE, true);
			start.passable = map[start.x][start.y].passable;
			goal.passable = map[goal.x][goal.y].passable;
			if(start.passable && goal.passable){
				open.push(start);
				start.visited = true;
				while (open.length > 0) {
					//Find lowest f, this is the current MapTile
					var lowestIndex:int = 0;
					for(var x:int = 0; x < open.length; x++){
						if(open[x].f < open[lowestIndex].f){
							lowestIndex = x;
						}
					}
					var currentMapTile:MapTile = open[lowestIndex];
					
					//End Case, Path has been found
					if(currentMapTile.x == goal.x && currentMapTile.y == goal.y){
						var path:Array = new Array();
						while(currentMapTile.parent != null){
							path.push(currentMapTile);
							currentMapTile = currentMapTile.parent;
						}
						//End of function
						return path;
					}
					
					//Normal Case, Continue searching
					open.splice(currentMapTile,1);
					currentMapTile.closed = true;
					var neighbors:Array = findNeighbors(currentMapTile);
					for(var a:int = 0; a < neighbors.length; a++){
						var neighbor:MapTile = neighbors[a];

						if(neighbor.closed || !neighbor.passable){
							//This one fails, move on
							continue;
						}
						//Check g score, and make sure its the best so far
						var gScore:int = currentMapTile.g + 1;
						var gScoreIsBest:Boolean = false;
						
						if(!neighbor.visited){
							//First time found MapTile, compute h
							
							//bulletSprite = new Sprite();
							//var other_bitmap:Bitmap = new pathTex();
							//bulletSprite.addChild(other_bitmap);
							//var testMatrix:Matrix = new Matrix();
							//testMatrix.translate(neighbor.x * Game.TILESIZE + 20, neighbor.y * Game.TILESIZE + 20);
							//Game.Renderer.draw(bulletSprite, testMatrix);	
							
							gScoreIsBest = true;
							neighbor.h = findH(neighbor.x, neighbor.y, goal.x, goal.y);
							open.push(neighbor);
							neighbor.visited = true;
						}
						else if(gScore < neighbor.g){
							//Already saw MapTile, but g score was worse before
							gScoreIsBest = true;
						}
						
						if(gScoreIsBest){
							//Found optimal path so far. Store info
							neighbor.parent = currentMapTile;
							neighbor.g = gScore;
							neighbor.f = neighbor.g + neighbor.h;
							
						}
					}
				}
				//No path found
				//trace("No path found");
				return null;
			}
			else{
			//Goal is not passable
			//trace("Passable.. Start:" + start.passable + " Goal: " + goal.passable);
			return null;
			}
		}
		
		public static function findNeighbors(curMapTile:MapTile):Array {
			var curNeighbors:Array = new Array();
			
			var x:int = curMapTile.x;
			var y:int = curMapTile.y;
			
			if(x-1 >= 0) {
				curNeighbors.push(map[x-1][y]);
			}
			if(x+1 < 16) {
				curNeighbors.push(map[x+1][y]);
			}
			if(y-1 >= 0) {
				curNeighbors.push(map[x][y-1]);
			}
			if (y + 1 < 12 ) {
				curNeighbors.push(map[x][y+1]);
			}
			
			return curNeighbors;
		}
		public static function findH(x1:int, x2:int, y1:int, y2:int):int {
			//Manhattan Distance
			return Math.abs(x1 - x2) + Math.abs(y1 - y2);
		}
	}
}