package 
{
	import adobe.utils.CustomActions;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Shader;
	import flash.display.Shape;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.getDefinitionByName;
	
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Preloader extends MovieClip 
	{
		[Embed(source = "Resources/Images/Preloader/explosion.png")]
		private var explosionTex:Class;
		
		[Embed(source = "Resources/Images/Preloader/grandma.png")]
		private var grandmaTex:Class;
		
		[Embed(source = "Resources/Images/Preloader/rocket.png")]
		private var rocketTex:Class;
		
		[Embed(source = "Resources/Images/Preloader/zombie.png")]
		private var zombieTex:Class;
		
		[Embed(source = "Resources/Images/Preloader/loading.gif")]
		private var loadingTex:Class;
		
		private var bit:Bitmap;

		public function Preloader() 
		{
			if (stage) {
				stage.scaleMode = StageScaleMode.NO_SCALE;
				stage.align = StageAlign.TOP_LEFT;
			}
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO show loader
			bit = new Bitmap();
			bit.bitmapData = new BitmapData(640, 480, false, 0x000000);
			
			bit.bitmapData.draw(new loadingTex().bitmapData, new Matrix(1, 0, 0, 1, 280, 200));
			bit.bitmapData.draw(new grandmaTex().bitmapData, new Matrix(1, 0, 0, 1, 10, 220));
			bit.bitmapData.draw(new rocketTex().bitmapData, new Matrix(1, 0, 0, 1, 10, 220));
			bit.bitmapData.draw(new zombieTex().bitmapData, new Matrix(1, 0, 0, 1, 610, 220));
			
			addChild(bit);
		}
		
		private function ioError(e:IOErrorEvent):void 
		{
			trace(e.text);
		}
		
		private function progress(e:ProgressEvent):void 
		{
			// TODO update loader
			var percent:Number = e.bytesLoaded / e.bytesTotal;
			bit.bitmapData.fillRect(new Rectangle(0, 0, 640, 480), 0x000000);
			bit.bitmapData.draw(new grandmaTex().bitmapData, new Matrix(1, 0, 0, 1, 10, 220));
			bit.bitmapData.draw(new loadingTex().bitmapData, new Matrix(1, 0, 0, 1, 280, 200));
			bit.bitmapData.draw(new rocketTex().bitmapData, new Matrix(1, 0, 0, 1, 10 + int(percent * 600), 220));
			bit.bitmapData.draw(new zombieTex().bitmapData, new Matrix(1, 0, 0, 1, 610, 220));			
		}
		
		private function checkFrame(e:Event):void 
		{
			if (currentFrame == totalFrames) 
			{
				stop();
				loadingFinished();
			}
		}
		
		private function loadingFinished():void 
		{
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			loaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, ioError);
			
			// TODO hide loader
			bit.bitmapData.draw(new explosionTex().bitmapData, new Matrix(1, 0, 0, 1, 600, 220));		
			removeChildAt(0);
			startup();
		}
		
		private function startup():void 
		{
			var mainClass:Class = getDefinitionByName("Main") as Class;
			addChild(new mainClass() as DisplayObject);
		}
		
	}
	
}