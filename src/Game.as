package  
{
	import Entities.Grandma;
	import Entities.Gun;
	import Entities.Item;
	import Entities.Marking;
	import Entities.Obstacle;
	import Entities.Projectile;
	import Entities.Zombie;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Scene;
	import flash.display.Sprite;
	import flash.errors.StackOverflowError;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.filters.BitmapFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.media.SoundMixer;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.utils.getTimer;
	import UI.Screen;
	import flash.ui.Mouse;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Game {
		//Variables
		public var bitmap:Bitmap;
		public static var Renderer:BitmapData;	
		
		[Embed(source = "Resources/Images/background2.gif")]
		private var background:Class;
		private var backgroundSprite:Sprite;
		
		[Embed(source = "Resources/Images/mainMenuBackground.png")]
		private var mainMenuBackground:Class;
		
		[Embed(source = "Resources/Images/pauseBackground.png")]
		private var pauseBackground:Class;
		
		[Embed(source = "Resources/Images/button.png")]
		private var buttonTex:Class;
		
		[Embed(source = "Resources/Images/buttonHover.png")]
		private var buttonHoverTex:Class;
		
		[Embed(source = "Resources/Images/crosshairs.png")]
		private var crosshairsTex:Class;
		private var crosshairsSprite:Sprite;
		private var crosshairsMatrix:Matrix;
		
		//Controls
		[Embed(source = "Resources/Images/arrows.gif")]
		private var arrowsTex:Class;
		
		[Embed(source = "Resources/Images/19scroll.gif")]
		private var scrollTex:Class;
		
		[Embed(source = "Resources/Images/aimfire.gif")]
		private var aimTex:Class;
		
		//Music
		[Embed(source="Resources/Audio/MoreSist-SoM-8941_fixed-loop.mp3")]
		private var backgroundMusic:Class;
		
		//Font
		[Embed(source="Resources/Fonts/ARCENA.TTF", 
			fontName = "myFont", 
			mimeType = "application/x-font", 
			fontWeight="normal", 
			fontStyle="normal", 
			unicodeRange = "U+0020-U+0040,U+0041-U+005A,U+005B-U+0060,U+0061-U+007A,U+007B-U+007E", 
			advancedAntiAliasing="true", 
			embedAsCFF = "false")]
			private var DontUseMe:Class; 
		
		[Embed(source = "Resources/Images/circle.png")]
		private var circleTex:Class;
		
		[Embed(source = "Resources/Images/circleShadow.png")]
		private var circleShadowTex:Class;
		private var circleShadowBitmap:Bitmap;
		
		[Embed(source = "Resources/Images/selected.png")]
		private var selectedTex:Class;
		private var selectedBitmap:Bitmap;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle1.png")]
		private var smallCircle1Tex:Class;
		
		//Audio
		[Embed(source="Resources/Audio/Pistol3.mp3")]
		private var weapon1Snd:Class;
		private var weapon1Sound:Sound;	
		
		[Embed(source = "Resources/Audio/SMG.mp3")]
		private var weapon2Snd:Class;
		private var weapon2Sound:Sound;	
		
		[Embed(source = "Resources/Audio/shotgun.mp3")]
		private var weapon3Snd:Class;
		private var weapon3Sound:Sound;			
		
		[Embed(source="Resources/Audio/machinegun(clipped).mp3")]
		private var weapon4Snd:Class;
		private var weapon4Sound:Sound;
		
		[Embed(source = "Resources/Audio/Sniper.mp3")]
		private var weapon5Snd:Class;
		private var weapon5Sound:Sound;	
		
		[Embed(source = "Resources/Audio/rocketlaunch.mp3")]
		private var weapon6Snd:Class;
		private var weapon6Sound:Sound;
		
		[Embed(source = "Resources/Audio/lazer.mp3")]
		private var weapon7Snd:Class;
		private var weapon7Sound:Sound;
		
		[Embed(source = "Resources/Audio/spray.mp3")]
		private var weapon8Snd:Class;
		private var weapon8Sound:Sound;	
		
		[Embed(source = "Resources/Audio/no ammo.mp3")]
		private var noAmmoSnd:Class;
		private var noAmmoSound:Sound;
		
		[Embed(source = "Resources/Audio/ammoPickup.mp3")]
		private var ammoPickupSnd:Class;
		private var ammoPickupSound:Sound;
		
		[Embed(source = "Resources/Audio/explosion_medium.mp3")]
		private var explosionSnd:Class;
		private var explosionSound:Sound;		
		
		//Gun Images
		[Embed(source = "Resources/Images/Guns/pistolSide.png")]
		private var weapon1Tex:Class;
		private var weapon1Bitmap:Bitmap
		
		[Embed(source = "Resources/Images/Guns/machinegunSide.png")]
		private var weapon2Tex:Class;
		private var weapon2Bitmap:Bitmap
		
		[Embed(source = "Resources/Images/Guns/shotgunSide.png")]
		private var weapon3Tex:Class;
		private var weapon3Bitmap:Bitmap
		
		[Embed(source = "Resources/Images/Guns/doubleMachinegunSide.png")]
		private var weapon4Tex:Class;
		private var weapon4Bitmap:Bitmap
		
		[Embed(source = "Resources/Images/Guns/sniperSide.png")]
		private var weapon5Tex:Class;
		private var weapon5Bitmap:Bitmap
		
		[Embed(source = "Resources/Images/Guns/rocketLauncherSide.png")]
		private var weapon6Tex:Class;
		private var weapon6Bitmap:Bitmap
		
		[Embed(source = "Resources/Images/Guns/railGunSide.png")]
		private var weapon7Tex:Class;
		private var weapon7Bitmap:Bitmap
		
		[Embed(source = "Resources/Images/Guns/pepperSpraySide.png")]
		private var weapon8Tex:Class;
		private var weapon8Bitmap:Bitmap
		
		[Embed(source="Resources/Images/healthPack.png")]
		private var healthPackTex:Class;
		private var healthPackBitmap:Bitmap;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle2.png")]
		private var smallCircle2Tex:Class;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle3.png")]
		private var smallCircle3Tex:Class;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle4.png")]
		private var smallCircle4Tex:Class;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle5.png")]
		private var smallCircle5Tex:Class;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle6.png")]
		private var smallCircle6Tex:Class;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle7.png")]
		private var smallCircle7Tex:Class;
		
		[Embed(source = "Resources/Images/SmallCircles/smallCircle8.png")]
		private var smallCircle8Tex:Class;
		
		[Embed(source = "Resources/Images/health.png")]
		private var healthTex:Class;
		
		[Embed(source = "Resources/Images/healthEmpty.png")]
		private var healthEmptyTex:Class;
		
		[Embed(source = "Resources/Images/mute.png")]
		private var muteTex:Class;
		
		[Embed(source = "Resources/Images/mutehovered.png")]
		private var muteHoverTex:Class;
		
		private var obstacles:Vector.<Obstacle>;
		public static var map:Vector.<Vector.<MapTile>>;
		public static const TILESIZE:int = 40;
		
		public static var grandma:Grandma;
		private var gun:Gun;

		private var projectiles:Vector.<Projectile>;
		private var lastBullet:int = 0;
		private const MAX_BULLET_AGE:int = 10000;
		private var selected:int = 0;
		private var unlimited:Boolean = false;
		
		private var zombies:Vector.<Zombie>;
		private var zombieCount:int = 0;
		private var baseZombieCount:int = 8;//use this to set intitial
		private var waveNumber:int = 0;
		private var wavePauseTimer:int = 0;
		private var waveSwitch:Boolean = true;
		
		private var markings:Vector.<Marking>;
		
		private var items:Vector.<Item>;
		
		public static var mousePos:Point;
		public static var mouse_down:Boolean;
		public static var mouse_click:Boolean;
		
		private var keys_down:Vector.<uint>;
		private const LEFT:int = 37;
		private const UP:int = 38;
		private const RIGHT:int = 39;
		private const DOWN:int = 40;
		private const W:int = 87;
		private const A:int = 65;
		private const S:int = 83;
		private const D:int = 68;
		private const Q:int = 81;
		private const E:int = 69;
		private const SPACE:int = 32;
		private const ESC:int = 27;
		private const ONE:int = 49;
		private const TWO:int = 50;
		private const THREE:int = 51;
		private const FOUR:int = 52;
		private const FIVE:int = 53;
		private const SIX:int = 54;
		private const SEVEN:int = 55;
		private const EIGHT:int = 56;
		
		public static var state:int = 0;
		
		private var health:int = 100;
		private var healthBitmap:Bitmap;
		private var healthDisplay:TextField;
		private var healthMatrix:Matrix;
		
		public static var kills:int = 0;
		private var killDisplay:TextField;
		private var killMatrix:Matrix;
		
		private var curGun:String = "";
		private var gunDisplay:TextField;
		private var gunMatrix:Matrix;
		private var fadeTime:int;
		
		private var ammoDisplay:TextField;
		private var ammoMatrix:Matrix;
		private var ammo:Array;
		
		private var getOutDisplay:TextField;
		private var getOutMatrix:Matrix;
		private var getOutTime:int;
		
		private var gameStarted:Boolean = false;
		
		public static const MAIN_MENU:int = 0;
		public static const PAUSE_MENU:int = 1;
		public static const GAME:int = 2;
		public static const GAME_OVER:int = 3;
		public static const CONTROLS:int = 4;
		
		private var mainMenuScreen:Screen;
		private var controlScreen:Screen;
		private var pauseMenuScreen:Screen;
		private var gameScreen:Screen;
		private var gameOverScreen:Screen;
		
		private var lastTime:int = 0;
		private var frames:int = 0;
		public static var FPS:int = 30;
		public static var deltaTime:Number = 1 / 30;
		//private var frameDisplay:TextField;	//REMOVE
		//private var frameMatrix:Matrix; //REMOVE
		
		private var muted:Boolean = false;
		
		//Constructor
		public function Game(stageWidth: int, stageHeight: int) {
			//Visual
			Renderer = new BitmapData(stageWidth, stageHeight, false, 0x000000);
			bitmap = new Bitmap(Renderer);
			
			backgroundSprite = new Sprite();
			var temp_bitmap:Bitmap = new background();
			backgroundSprite.addChild(temp_bitmap);	
			
			//Custom Mouse
			Mouse.hide();
			crosshairsSprite = new Sprite();
			crosshairsSprite.addChild(new crosshairsTex());
			crosshairsSprite.mouseEnabled = false;
			crosshairsSprite.mouseChildren = false;
			crosshairsMatrix = new Matrix();
			crosshairsMatrix.translate( -20, -20);
			
			//Map
			markings = new Vector.<Marking>;
			createMap();
			
			//Entities
			spawnGrandma();
			gun = new Gun(0,0,TILESIZE,TILESIZE);
			projectiles = new Vector.<Projectile>;
			zombies = new Vector.<Zombie>;
			items = new Vector.<Item>;
			ammo = new Array("Infinite", 0, 0, 0, 0, 0, 0, 0);
			
			//Input
			mousePos = new Point(0, 0);
			mouse_down = false;
			mouse_click = false;
			keys_down = new Vector.<uint>;
			
			//Main Menu Screen
			var tempBackground:Bitmap = new mainMenuBackground();
			mainMenuScreen = new Screen(170, 0, 300, Renderer.height, false, 0x000000, true, tempBackground.bitmapData);
			var format3:TextFormat = new TextFormat("myFont", 20, 0x000000, true);
			format3.align = "center";
			var buttonBit:Bitmap = new buttonTex();
			var buttonHoverBit:Bitmap = new buttonHoverTex();
			mainMenuScreen.addImageButton(233, 140, buttonBit.bitmapData, buttonHoverBit.bitmapData, startNewGame, "Start Game", format3);
			mainMenuScreen.addImageButton(233, 190, buttonBit.bitmapData, buttonHoverBit.bitmapData, startUnlimitedGame, "Unlimited Ammo Game", format3);
			mainMenuScreen.addImageButton(233, 290, buttonBit.bitmapData, buttonHoverBit.bitmapData, showControls, "Controls", format3);
			mainMenuScreen.addImageButton(233, 240, buttonBit.bitmapData, buttonHoverBit.bitmapData, startGame, "Resume Game", format3);
			var muteBit:Bitmap = new muteTex();
			var muteHoverBit:Bitmap = new muteHoverTex();
			mainMenuScreen.addImageButton(Renderer.width - 30, Renderer.height - 30, muteBit.bitmapData, muteHoverBit.bitmapData, toggleMute);

			//Control Screen
			controlScreen = new Screen(170, 0, 300, Renderer.height, false, 0x000000, true, tempBackground.bitmapData);
			controlScreen.AddText(0, 80, controlScreen.width, 100, "Controls", format3);
			controlScreen.addImage(240, 140, new arrowsTex().bitmapData);
			controlScreen.addImage(285, 200, new aimTex().bitmapData);
			controlScreen.addImage(240, 270, new scrollTex().bitmapData);
			controlScreen.addImageButton(233, 340, buttonBit.bitmapData, buttonHoverBit.bitmapData, startNewGame, "New Game", format3);
			controlScreen.addImageButton(233, 390, buttonBit.bitmapData, buttonHoverBit.bitmapData, startGame, "Resume Game", format3);
			controlScreen.addImageButton(233, 440, buttonBit.bitmapData, buttonHoverBit.bitmapData, startMainMenu, "Main Menu", format3);
			
			//Pause Menu Screen
			var pauseBit:Bitmap = new pauseBackground();
			pauseMenuScreen = new Screen(213, 120, 215, 250, false, 0x000000, true, pauseBit.bitmapData);
			pauseMenuScreen.AddText(0, 40, pauseMenuScreen.width, 100, "Paused", format3);
			pauseMenuScreen.addImageButton(233, 220, buttonBit.bitmapData, buttonHoverBit.bitmapData, startGame, "Return to Game", format3);
			pauseMenuScreen.addImageButton(233, 270, buttonBit.bitmapData, buttonHoverBit.bitmapData, showControls, "Controls", format3);
			pauseMenuScreen.addImageButton(233, 320, buttonBit.bitmapData, buttonHoverBit.bitmapData, startMainMenu, "Go to Menu", format3);
			pauseMenuScreen.addImageButton(Renderer.width - 30, Renderer.height - 30, muteBit.bitmapData, muteHoverBit.bitmapData, toggleMute);
			
			//Game Screen
			gameScreen = new Screen(0, 0, Renderer.width, Renderer.height, false, 0x111111);
			var format5:TextFormat = new TextFormat("myFont", 20, 0xFFFFFF, false);
			format5.align = "left";
			gameScreen.AddText(10, 5, 80, 50, "Kills:", format5);
			gameScreen.AddText(Renderer.width - 60, 5, 50, 50, "Ammo:", format5);
			format5.align = "center";
			killDisplay = new TextField();
			killDisplay.embedFonts = true;
			killDisplay.defaultTextFormat = format5;
			gunDisplay = new TextField();
			gunDisplay.embedFonts = true;
			gunDisplay.width = 200;
			gunDisplay.defaultTextFormat = format5;
			ammoDisplay = new TextField();
			ammoDisplay.embedFonts = true;
			ammoDisplay.defaultTextFormat = format5;
			getOutDisplay = new TextField();
			getOutDisplay.embedFonts = true;
			getOutDisplay.width = 10;
			getOutDisplay.defaultTextFormat = format5;
			getOutDisplay.text = "GET OUTTA MY GARDEN!!!";
			
			//frameDisplay = new TextField();			//REMOVE ALL 4
			//frameDisplay.embedFonts = true;
			//frameDisplay.defaultTextFormat = format5;
			//frameMatrix = new Matrix(1, 0, 0, 1, 200, 200);
			
			killMatrix = new Matrix(1, 0, 0, 1, -20, 30);
			gunMatrix = new Matrix(1, 0, 0, 1, Renderer.width / 2 - 100, Renderer.height - 80);
			ammoMatrix = new Matrix(1, 0, 0, 1, Renderer.width - 85, 30);
			getOutMatrix = new Matrix(1, 0, 0, 1, Renderer.width/2 - 95, Renderer.height/2 - 120);
			var smallCirc1Bitmap:Bitmap = new smallCircle1Tex();
			gameScreen.addImage(122 + 50 * 0, Renderer.height - 55, smallCirc1Bitmap.bitmapData);
			var smallCirc2Bitmap:Bitmap = new smallCircle2Tex();
			gameScreen.addImage(122 + 50*1, Renderer.height - 55, smallCirc2Bitmap.bitmapData);
			var smallCirc3Bitmap:Bitmap = new smallCircle3Tex();
			gameScreen.addImage(122 + 50*2, Renderer.height - 55, smallCirc3Bitmap.bitmapData);
			var smallCirc4Bitmap:Bitmap = new smallCircle4Tex();
			gameScreen.addImage(122 + 50*3, Renderer.height - 55, smallCirc4Bitmap.bitmapData);
			var smallCirc5Bitmap:Bitmap = new smallCircle5Tex();
			gameScreen.addImage(122 + 50*4, Renderer.height - 55, smallCirc5Bitmap.bitmapData);
			var smallCirc6Bitmap:Bitmap = new smallCircle6Tex();
			gameScreen.addImage(122 + 50*5, Renderer.height - 55, smallCirc6Bitmap.bitmapData);
			var smallCirc7Bitmap:Bitmap = new smallCircle7Tex();
			gameScreen.addImage(122 + 50*6, Renderer.height - 55, smallCirc7Bitmap.bitmapData);
			var smallCirc8Bitmap:Bitmap = new smallCircle8Tex();
			gameScreen.addImage(122 + 50 * 7, Renderer.height - 55, smallCirc8Bitmap.bitmapData);
			var circBitmap:Bitmap = new circleTex();
			for (var d:int = 0; d < 8; d++ ) {
				gameScreen.addImage(125 + 50*d, Renderer.height - 45, circBitmap.bitmapData);
			}
			circleShadowBitmap = new circleShadowTex();
			weapon1Bitmap = new weapon1Tex();
			weapon2Bitmap = new weapon2Tex();
			weapon3Bitmap = new weapon3Tex();
			weapon4Bitmap = new weapon4Tex();
			weapon5Bitmap = new weapon5Tex();
			weapon6Bitmap = new weapon6Tex();
			weapon7Bitmap = new weapon7Tex();
			weapon8Bitmap = new weapon8Tex();
			healthPackBitmap = new healthPackTex();
			gameScreen.addImage(139 + 50 * 0, Renderer.height - 30, weapon1Bitmap.bitmapData);
			gameScreen.addImage(139 + 50 * 1, Renderer.height - 30, weapon2Bitmap.bitmapData);
			gameScreen.addImage(139 + 50 * 2, Renderer.height - 30, weapon3Bitmap.bitmapData);
			gameScreen.addImage(139 + 50 * 3, Renderer.height - 30, weapon4Bitmap.bitmapData);
			gameScreen.addImage(137 + 50 * 4, Renderer.height - 30, weapon5Bitmap.bitmapData);
			gameScreen.addImage(139 + 50 * 5, Renderer.height - 30, weapon6Bitmap.bitmapData);
			gameScreen.addImage(139 + 50 * 6, Renderer.height - 30, weapon7Bitmap.bitmapData);
			gameScreen.addImage(138 + 50 * 7, Renderer.height - 30, weapon8Bitmap.bitmapData);
			selectedBitmap = new selectedTex();		
			healthBitmap = new healthTex();
			healthDisplay = new TextField();
			healthDisplay.embedFonts = true;
			healthDisplay.defaultTextFormat = format5;
			healthMatrix = new Matrix(1, 0, 0, 1, 250, 5);
			var healthEmptyBitmap:Bitmap = new healthEmptyTex();
			gameScreen.addImage(250, 5, healthEmptyBitmap.bitmapData);
			gameScreen.addImageButton(Renderer.width - 30, Renderer.height - 30, muteBit.bitmapData, muteHoverBit.bitmapData, toggleMute);
			
			//Game Over Screen
			gameOverScreen = new Screen(213, 120, 215, 250, false, 0x000000, true, pauseBit.bitmapData);
			gameOverScreen.AddText(0, 40, pauseMenuScreen.width, 100, "Game Over", format3);
			gameOverScreen.addImageButton(233, 220, buttonBit.bitmapData, buttonHoverBit.bitmapData, retryGame, "Retry", format3);
			gameOverScreen.addImageButton(233, 270, buttonBit.bitmapData, buttonHoverBit.bitmapData, showControls,"Controls", format3);
			gameOverScreen.addImageButton(233, 320, buttonBit.bitmapData, buttonHoverBit.bitmapData, startMainMenu, "Go to Menu", format3);
			gameOverScreen.addImageButton(Renderer.width - 30, Renderer.height - 30, muteBit.bitmapData, muteHoverBit.bitmapData, toggleMute);
			
			//Sounds
			var gameMusic:Sound = new backgroundMusic();
			var musicTransform:SoundTransform = new SoundTransform(.3, 0);
			gameMusic.play(.027, int.MAX_VALUE, musicTransform);
			weapon1Sound = new weapon1Snd();
			weapon2Sound = new weapon2Snd();
			weapon3Sound = new weapon3Snd();
			weapon4Sound = new weapon4Snd();
			weapon5Sound = new weapon5Snd();
			weapon6Sound = new weapon6Snd();
			weapon7Sound = new weapon7Snd();
			weapon8Sound = new weapon8Snd();
			noAmmoSound = new noAmmoSnd();
			ammoPickupSound = new ammoPickupSnd();
			explosionSound = new explosionSnd();
			
		}
		
		//Functions
		public function update():void {
			frames++;
			
			if (getTimer() - lastTime > 1000) {
				FPS = frames;
				deltaTime = 1 / 30;
				frames = 0;
				lastTime = getTimer();
			}
			
			if (state == GAME) {
				gameStarted = true;
				
				if (health <= 0){
					state = GAME_OVER;
					return;
				}
				
				if (zombieCount == 0 && waveSwitch) {
					wavePauseTimer = getTimer()
					waveSwitch = false;
				}
				
				if (zombieCount == 0 && getTimer() - wavePauseTimer > 3000) {
					waveNumber++;
					if (waveNumber == 1) zombieCount = 3;//First Level
					else {zombieCount = baseZombieCount * Math.pow(1.15, waveNumber - 1);}
					waveSwitch = true;
				}
				
				if (checkKeyDown(ESC)) startPauseMenu();
				if (checkKeyDown(LEFT) || checkKeyDown(A))grandma.moveLeft();
				if (checkKeyDown(RIGHT) || checkKeyDown(D))grandma.moveRight();
				if (checkKeyDown(UP) || checkKeyDown(W))grandma.moveUp();
				if (checkKeyDown(DOWN) || checkKeyDown(S)) grandma.moveDown();
				if (checkKeyDown(Q) && getTimer() - lastBullet > 200) { selected--; if (selected < 0) selected = 0; lastBullet = getTimer(); }
				if (checkKeyDown(E) && getTimer() - lastBullet > 200) { selected++; if (selected > 7) selected = 7; lastBullet = getTimer(); }
				if (checkKeyDown(ONE)) { selected = 0; curGun = "Pistol"; fadeTime = getTimer();}
				if (checkKeyDown(TWO)) { selected = 1; curGun = "Submachine Gun"; fadeTime = getTimer();}
				if (checkKeyDown(THREE)) { selected = 2; curGun = "Shotgun"; fadeTime = getTimer();}
				if (checkKeyDown(FOUR)) { selected = 3; curGun = "Machine Gun"; fadeTime = getTimer();}
				if (checkKeyDown(FIVE)) { selected = 4; curGun = "Sniper Rifle"; fadeTime = getTimer();}
				if (checkKeyDown(SIX)) { selected = 5; curGun = "Rocket Launcher"; fadeTime = getTimer();}
				if (checkKeyDown(SEVEN)) { selected = 6; curGun = "Rail Gun"; fadeTime = getTimer();}
				if (checkKeyDown(EIGHT)) { selected = 7; curGun = "Pepper Spray"; fadeTime = getTimer();}
				gun.type = selected;
				if (getTimer() - lastBullet > 200) {
					var xShift:int = gun.matrix.tx;
					var yShift:int = gun.matrix.ty;
					var angle:Number = Math.atan((mousePos.y - yShift + 10) / (mousePos.x - xShift + 10))
					if (mousePos.x < xShift - 10) angle += Math.PI //arctan only 180 degree range
					if (mouse_click) {
						//Pistol
						if (selected == 0) {
							weapon1Sound.play();
							lastBullet = getTimer();
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 300, angle));
						}
						//Shotgun
						if (selected == 2) {
							if(unlimited || ammo[2] > 0){
								if(!unlimited)ammo[2]--;
								weapon3Sound.play();
								lastBullet = getTimer();
								projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 300, angle - Math.PI / 18));
								projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 300, angle));
								projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 300, angle + Math.PI / 18));
							}
							else { noAmmoSound.play(); getTimer(); shiftToAmmo(2)}
						}
					}
					if (mouse_down) {
						//Machine Gun
						if (selected == 1) {
							if(unlimited || ammo[1] > 0){
								if(!unlimited)ammo[1]--;
								weapon2Sound.play();
								lastBullet = getTimer();
								projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 300, angle));			
							}
							else { noAmmoSound.play(); getTimer(); shiftToAmmo(1)}
						}			
						//Double Machine Gun
						if (selected == 3) {
							if(unlimited || ammo[3] > 0){
								if(!unlimited)ammo[3]-=2;
								weapon4Sound.play();
								lastBullet = getTimer();
								projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 300, angle));							
								projectiles.push(new Projectile(xShift + 10, yShift, 5, 5, getTimer(), 300, angle));
							}
							else { noAmmoSound.play(); getTimer(); shiftToAmmo(3)}
						}
					}
				}
				if (mouse_click && getTimer() - lastBullet > 500) {
					var xShift:int = gun.matrix.tx;
					var yShift:int = gun.matrix.ty;
					var angle:Number = Math.atan((mousePos.y - yShift + 10) / (mousePos.x - xShift + 10))
					if(mousePos.x < xShift - 10)angle += Math.PI //arctan only 180 degree range					
					//Sniper
					if (selected == 4) {
						if(unlimited || ammo[4] > 0){
							if(!unlimited)ammo[4]--;
							weapon5Sound.play();
							lastBullet = getTimer();
							projectiles.push(new Projectile(xShift, yShift, 3, 10, getTimer(), 400, angle, "big bullet"));
						}
						else { noAmmoSound.play(); getTimer(); shiftToAmmo(4)}
					}
					//Rocket
					if (selected == 5) {
						if(unlimited || ammo[5] > 0){
							if(!unlimited)ammo[5]--;
							weapon6Sound.play();
							lastBullet = getTimer();
							projectiles.push(new Projectile(xShift, yShift, 5, 10, getTimer(), 150, angle, "rocket"));
						}
						else { noAmmoSound.play(); getTimer(); shiftToAmmo(5)}
					}
					//Pepper Spray
					if (selected == 7) {
						if(ammo[7] > 0){
							ammo[7]--;
							weapon8Sound.play();
							lastBullet = getTimer();
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI*3/4, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI/2, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI/4 , "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle + Math.PI*3/4, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle + Math.PI/2, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle + Math.PI/4, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle, "pepper"));
							
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI*7/8, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI*5/8, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI*3/8, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle - Math.PI/8, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle + Math.PI*7/8, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle + Math.PI*5/8, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle + Math.PI*3/8, "pepper"));
							projectiles.push(new Projectile(xShift, yShift, 5, 5, getTimer(), 600, angle + Math.PI/8, "pepper"));						
							
							for (var g:int = 0; g < zombies.length; g++ ) {
								zombies[g].health = 0;
							}
						}
						else { noAmmoSound.play(); getTimer(); shiftToAmmo(7)}
					}					
					//RailGun
					else if (selected == 6) {
						if(unlimited || ammo[6] > 0){
							if(!unlimited)ammo[6]--;
							weapon7Sound.play();
							lastBullet = getTimer();
							projectiles.push(new Projectile(xShift, yShift, 10, 820, getTimer() - MAX_BULLET_AGE + 100, 500, angle, "laser"));
						}
						else { noAmmoSound.play(); getTimer(); shiftToAmmo(6)}
					}
				}

				if (zombies.length < zombieCount) {
					if (Math.random() < .5) {
						if (Math.random() < .5) zombies.push(new Zombie(-TILESIZE, Math.random() * Renderer.height, TILESIZE, TILESIZE));
						else zombies.push(new Zombie(Renderer.width, Math.random() * Renderer.height, TILESIZE, TILESIZE));
					}
					else {
						if (Math.random() < .5) zombies.push(new Zombie(Math.random() * Renderer.width, -TILESIZE, TILESIZE, TILESIZE));
						else zombies.push(new Zombie(Math.random() * Renderer.width, Renderer.height, TILESIZE, TILESIZE));
					}
				}
				
				var GAngle:Number = Math.atan((mousePos.y - (grandma.y + 10)) / (mousePos.x - (grandma.x + 10)))
				if (mousePos.x < (grandma.x + 10)) GAngle += Math.PI //arctan only 180 degree range
				grandma.angle = GAngle;
				grandma.update();
				
				for (var i:int = 0; i < projectiles.length; i++ ) {
					projectiles[i].update();
					
					if (getTimer() - projectiles[i].age > MAX_BULLET_AGE ){
						if (projectiles[i].type == "rocket"){
							markings.push(new Marking(projectiles[i].x - TILESIZE / 2, projectiles[i].y - TILESIZE / 2, getTimer(), "explosion"));
							explosionSound.play();
						}
						projectiles.splice(i, 1);
						break;
					}
					
					if (projectiles[i].type == "pepper") continue;
					
					for (var j:int = 0; j < zombies.length; j++ ) {
						
						if (projectiles[i].type == "laser") {
							var removed:Boolean = checkLaserCollision(i, j);
							if (removed) break;
						}
						else{
							if (collided(projectiles[i].x, projectiles[i].y, projectiles[i].width, projectiles[i].height ,zombies[j].x, zombies[j].y, zombies[j].width, zombies[j].height)){
								if (projectiles[i].type == "rocket") {
									markings.push(new Marking(projectiles[i].x - TILESIZE / 2, projectiles[i].y - TILESIZE / 2, getTimer(), "explosion"));
									explosionSound.play();
								}
								if(projectiles[i].type != "big bullet")projectiles.splice(i, 1);
								zombies[j].health--;	//Kill logic below in explosions and lasers
								break;
							}
						}
					}
				}
				for (var k:int = 0; k < zombies.length; k++ ) {
					if (Math.abs(zombies[k].x - grandma.x) + Math.abs(zombies[k].y - grandma.y)  < 40)
						health--;
					zombies[k].update();
					if (zombies[k].health <= 0) killZombie(k);
				}
				
				for (var i:int = 0; i < markings.length; i++ ) {
					if(markings[i].type == "explosion"){
						for (var j:int = 0; j < zombies.length; j++ ) {
							if (collided(markings[i].x, markings[i].y, markings[i].width, markings[i].height ,zombies[j].x, zombies[j].y, zombies[j].width, zombies[j].height)){
								zombies[j].health = 0;		//Kill logic also in laserCollision below
								break;
							}	
						}
					}
					
					markings[i].update();	
						
					if (markings[i].remove){
						markings.splice(i, 1);
						break;
					}
				}
				
				for (var c:int = 0; c < items.length; c++ ) {
					if (getTimer() - items[c].life > 15000) { items.splice(c, 1); break;}
					if (collided(grandma.x, grandma.y, grandma.width, grandma.height, items[c].x, items[c].y, items[c].width, items[c].height)) {
						//Pick up item
						ammoPickupSound.play(0,0,new SoundTransform(.2,0));
						var t:int = items[c].type;
						if (t == 1) { health += 20; if (health > 100) health = 100; curGun = "Health +20"; fadeTime = getTimer()}
						else if (t == 2){ ammo[1] += 30; curGun = "Submachine Gun Ammo +30"; fadeTime = getTimer();}
						else if (t == 3){ ammo[2] += 15; curGun = "Shotgun Ammo +15"; fadeTime = getTimer();}
						else if (t == 4){ ammo[3] += 40; curGun = "Machine Gun Ammo +20"; fadeTime = getTimer();}
						else if (t == 5){ ammo[4] += 10; curGun = "Sniper Rifle Ammo +10"; fadeTime = getTimer();}
						else if (t == 6){ ammo[5] += 5; curGun = "Rocket Launcher Ammo +5"; fadeTime = getTimer();}
						else if (t == 7){ ammo[6] += 3;  curGun = "Rail Gun Ammo +3"; fadeTime = getTimer();}
						else if (t == 8){ ammo[7] += 1;  curGun = "Pepper Spray +1"; fadeTime = getTimer();}
						items.splice(c, 1);
						break;				
					}
				}

				if (getTimer() - fadeTime > 1000) {
					curGun = "";
				}
				
				gameScreen.update();
			} //End of Game State
			
			if (state == MAIN_MENU) {
				menuBackgroundUpdate();
				mainMenuScreen.update();
			}
			
			if (state == PAUSE_MENU) {
				pauseMenuScreen.update();
			}
			
			if (state == GAME_OVER) {
				gameOverScreen.update();
			}
			
			if (state == CONTROLS) {
				controlScreen.update();
			}
			
			if (muted) SoundMixer.stopAll();
			mouse_click = false; //Must be last
		}	
		
		public function render():void {
			
			Renderer.lock();
			
			renderGame();
			
			if (state == MAIN_MENU) {
				mainMenuScreen.render(Renderer);
			}
			
			if (state == PAUSE_MENU) {			
				pauseMenuScreen.render(Renderer);
			}
			
			if (state == GAME_OVER) {
				gameOverScreen.render(Renderer);
			}
			
			if (state == CONTROLS) {
				controlScreen.render(Renderer);
			}
			
			Renderer.draw(crosshairsSprite, crosshairsMatrix);
			
			Renderer.unlock();
		
		}
		
		public function startGame():void {
			if(gameStarted)
				state = GAME;
		}
		
		public function startNewGame():void {
			waveNumber = 0;
			zombieCount = 0;
			selected = 0;
			zombies = new Vector.<Zombie>;
			projectiles = new Vector.<Projectile>;
			markings = new Vector.<Marking>;
			kills = 0;
			items = new Vector.<Item>;
			createMap();
			spawnGrandma();
			health = 100;
			unlimited = false;
			ammo = new Array("Infinite", 0, 0, 0, 0, 0, 0, 0);
			getOutTime = getTimer();
			getOutDisplay.width = 10;
			
			state = GAME;
		}
		
		private function startUnlimitedGame():void {
			startNewGame();
			unlimited = true;
			ammo = new Array("Infinite", "Infinite", "Infinite", "Infinite", "Infinite", "Infinite", "Infinite", 0);
		}
		
		private function retryGame():void {
			var isItUnlimited:Boolean = unlimited;
			startNewGame();
			unlimited = isItUnlimited;
			if(unlimited)ammo = new Array("Infinite", "Infinite", "Infinite", "Infinite", "Infinite", "Infinite", "Infinite", 0);
		}
		
		public function startMainMenu():void {
			state = MAIN_MENU;
		}
		
		public function startPauseMenu():void {
			state = PAUSE_MENU;
		}
		
		public function showControls():void {
			state = CONTROLS;
		}
		
		public function createMap():void {
			map = new Vector.<Vector.<MapTile>>;
			for (var j:int = 0; j < Renderer.width / TILESIZE; j++ ) {
				map.push(new Vector.<MapTile>)
				for (var k:int = 0; k < Renderer.height / TILESIZE; k ++ ) {
					map[j].push(new MapTile(j, k, true ));
				}
			}
			obstacles = new Vector.<Obstacle>;
			for (var i:int = 0; i < 30; i++) {
				var xOb:int = (int)(Math.random() * Renderer.width/TILESIZE);
				var yOb:int = (int)(Math.random() * Renderer.height/TILESIZE);
				if (yOb == 6 && (xOb == 6 || xOb == 7 || xOb == 8 || xOb == 9)) yOb++;
				if (map[xOb][yOb].passable == false) { i--; continue; }
				map[xOb][yOb].passable = false;
				obstacles.push(new Obstacle(xOb * TILESIZE, yOb * TILESIZE, TILESIZE, TILESIZE));
			}
			markings.push(new Marking(Math.random() * (Renderer.width - 115), Math.random() * (Renderer.height-115), 1, "ground"));
			markings.push(new Marking(Math.random() * (Renderer.width - 115), Math.random() * (Renderer.height-115), 1, "ground"));
		}
		public function spawnGrandma():void{
			var spawnX:int = Renderer.width / 2 - TILESIZE/2;
			var spawnY:int = Renderer.height / 2;
			grandma = new Grandma(spawnX, spawnY, TILESIZE, TILESIZE);	
		}
		
		public function renderGame():void {
				Renderer.draw(backgroundSprite);
				
				for (var k:int = 0; k < markings.length; k++ ) {
					markings[k].render();
				}			
				
				for (var a:int = 0; a < obstacles.length; a++ ) {
					obstacles[a].render();
				}
				
				for (var b:int = 0; b < items.length; b++ ) {
					items[b].render();
				}
				
				grandma.render();
				
				gun.render();
				
				for (var i:int = 0; i < projectiles.length; i++ ) {
					projectiles[i].render();
				}			
				
				for (var j:int = 0; j < zombies.length; j++ ) {
					zombies[j].render();
				}
				
				//GUI all after this
				if(state != MAIN_MENU && state != CONTROLS){
					
					gameScreen.render(Renderer);
					
					killDisplay.text = kills.toString();
					Renderer.draw(killDisplay, killMatrix);
					
					gunDisplay.text = curGun;
					Renderer.draw(gunDisplay, gunMatrix);
					
					ammoDisplay.text = ammo[selected].toString();
					Renderer.draw(ammoDisplay, ammoMatrix);
					
					if (getTimer() - getOutTime < 2000) {
						if(getOutDisplay.width < 190)
							getOutDisplay.width = (getTimer() - getOutTime) * 300 / 2000
						Renderer.draw(getOutDisplay, getOutMatrix);
					}
					
					//frameDisplay.text = FPS.toString();			//REMOVE
					//Renderer.draw(frameDisplay, frameMatrix);
					
					var scale:Number = health / 100;
					Renderer.draw(healthBitmap, new Matrix(scale, 0, 0, 1, 250, 5));
					healthDisplay.text = "Health";
					Renderer.draw(healthDisplay, new Matrix(scale, 0, 0, 1, 250 + 20 * scale, 5));
					
					Renderer.draw(selectedBitmap, new Matrix(1,0,0,1,126 + 50 * selected, Renderer.height - 44));
					
					for (var f:int = 1; f < 8; f++ ) {
						if (ammo[f] == 0) {
							Renderer.draw(circleShadowBitmap, new Matrix(1, 0, 0, 1, 125 + 50 * f, Renderer.height - 45));
						}
					}
				}
				
					
		}
		
		private function dropItem(x:int, y:int):void {
			if(!unlimited && Math.random() > .9){
			var chance:Number = Math.random();
				if (chance > .70) {items.push(new Item(x,y, 2, weapon2Bitmap.bitmapData, getTimer())) ; return; } //SMG			30
				if (chance > .50) {items.push(new Item(x,y, 3, weapon3Bitmap.bitmapData, getTimer())) ; return; } //Shotgun		20
				if (chance > .30) {items.push(new Item(x,y, 4, weapon4Bitmap.bitmapData, getTimer())) ; return; } //MG			20
				if (chance > .22) {items.push(new Item(x,y, 5, weapon5Bitmap.bitmapData, getTimer())) ; return; } //Sniper		 8 
				if (chance > .14) {items.push(new Item(x,y, 6, weapon6Bitmap.bitmapData, getTimer())) ; return; } //Rockets		 8
				if (chance > .11) {items.push(new Item(x,y, 7, weapon7Bitmap.bitmapData, getTimer())) ; return; } //Rail		 3
				if (chance > .10) {items.push(new Item(x,y, 8, weapon8Bitmap.bitmapData, getTimer())) ; return; } //Pepper		 1
				else			  {items.push(new Item(x, y, 1, healthPackBitmap.bitmapData, getTimer())) ; return; } //Health  10
			}
			
			
		}
		
		private function shiftToAmmo(i:int):void {
			while (ammo[i] == 0) i--;
			selected = i;
			
			if (selected == 0) curGun = "Pistol";
			if (selected == 1) curGun = "Submachine Gun";
			if (selected == 2) curGun = "Shotgun";
			if (selected == 3) curGun = "Machine Gun"; 
			if (selected == 4) curGun = "Sniper Rifle"; 
			if (selected == 5) curGun = "Rocket Launcher";
			if (selected == 6) curGun = "Rail Gun";
			if (selected == 7) curGun = "Pepper Spray";
			fadeTime = getTimer();			
		}
		
		private function menuBackgroundUpdate():void {
			
		}
		
		public function toggleMute():void {
			if (muted == true) {
				var gameMusic:Sound = new backgroundMusic();
				var musicTransform:SoundTransform = new SoundTransform(.6, 0);
				gameMusic.play(.027, int.MAX_VALUE, musicTransform);				
			}
			muted = !muted;
		}
		
		private function checkLaserCollision(i:int, j:int):Boolean {
			var ret:Boolean = false;
			for (var a:int = 0; a < 820; a+=20 ) {
				var x:int = projectiles[i].x + Math.cos(projectiles[i].angleRads) * a;
				var y:int = projectiles[i].y + Math.sin(projectiles[i].angleRads) * a;
				if (collided(x, y, 20, 20, zombies[j].x, zombies[j].y, zombies[j].width, zombies[j].height)) {
					zombies[j].health = 0;
					break;					
				}
			}
			return ret;
		}
		
		public function mouseUp(e:MouseEvent):void {
			mouse_down = false;
		}
		
		public function mouseDown(e:MouseEvent):void {
			mouse_down = true;
			mouse_click = true;
		}
		
		public function mouseMove(e:MouseEvent):void {
			crosshairsMatrix = new Matrix();
			crosshairsMatrix.translate(e.stageX, e.stageY);
			mousePos.x = e.stageX;
			mousePos.y = e.stageY;
		}
		
		public function mouseWheel(e:MouseEvent):void {
			if (e.delta > 0) selected--;
			else if (e.delta < 0) selected++;
			if (selected < 0) selected = 0;
			if (selected > 7) selected = 7;
			
			if (selected == 0) curGun = "Pistol";
			if (selected == 1) curGun = "Submachine Gun";
			if (selected == 2) curGun = "Shotgun";
			if (selected == 3) curGun = "Machine Gun"; 
			if (selected == 4) curGun = "Sniper Rifle"; 
			if (selected == 5) curGun = "Rocket Launcher";
			if (selected == 6) curGun = "Rail Gun";
			if (selected == 7) curGun = "Pepper Spray";
			fadeTime = getTimer();
		}
		
		public function keyUp(e:KeyboardEvent):void {
			var key_pos:int = -1;
			//check if key released is in the array
			for (var i:int = 0; i < keys_down.length; i++) {
				if (e.keyCode == keys_down[i]) {
					key_pos = i;
					break;
				}
			}
			
			if (key_pos != -1) keys_down.splice(key_pos, 1);
		}
		
		public function keyDown(e:KeyboardEvent):void {
			var key_down:Boolean = false;
			//Check if already in pressed array
			for (var i:int = 0; i < keys_down.length; i++ ) {
				if (keys_down[i] == e.keyCode) key_down = true;
			}
			
			if (!key_down) keys_down.push(e.keyCode);
		}
		
		public function checkKeyDown(keycode:int):Boolean {
			var answer:Boolean = false;
			for (var i:int = 0; i < keys_down.length; i++ ) {
				if (keys_down[i] == keycode) {
					answer = true;
					break;
				}
			}
			return answer;
		}
		
		public static function collided(x1:int, y1:int, w1:int, h1:int, x2:int, y2:int, w2:int, h2:int):Boolean {
			if (x1 + w1 > x2 && x1 < x2 + w2)
				if (y1 + h1 > y2 && y1 < y2 + h2)
					return true;
			return false;
		}
		
		private function killZombie(j:int):void {
			markings.push(new Marking(zombies[j].x, zombies[j].y, getTimer(), "blood"));
			dropItem(zombies[j].x, zombies[j].y);
			zombies.splice(j, 1);
			zombieCount--;
			kills++;
		}
	}
}