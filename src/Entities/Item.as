package Entities 
{
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Item extends GameSprite {
		
		public var type:int;
		public var life:int;
		
		public function Item(xx:int, yy:int, type:int, image:BitmapData, life:int) {
			super(xx, yy, image.width, image.height);
			this.image = image;
			this.type = type;
			this.life = life;
		}
		
		override public function render():void {
			Game.Renderer.draw(image, new Matrix(1, 0, 0, 1, x, y));
		}
	}

}