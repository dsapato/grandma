package Entities 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Grandma extends GameSprite {
		[Embed(source="../Resources/Images/grandma.png")]
		private var tex:Class;
		private var bit:Bitmap;
		
		private var maxSpeed:int = 7;
		private var friction:Number = .7;
		private var acceleration:Number = 30;
		
		private var collisionPoints:Array;
		private const BUFFER:int = 5;
		
		public static var matrix:Matrix;
		public var angle:Number;
		
		private var animationCount:int = 0;
		private var animationIndex:int = 0;
		private var animRect:Rectangle;
		
		public function Grandma(xx:int, yy:int, w:int, h:int, angle:int = 0) {
			super(xx, yy, w, h);
			this.angle = angle;
			vx = 0;
			vy = 0;
			imageSprite = new Sprite();
			bit = new tex();
			matrix = new Matrix();
			animRect = new Rectangle(0, 0, Game.TILESIZE, Game.TILESIZE)
		}
		
		override public function update():void {
			
			//X Movement
			if (vx != 0) {
				var pass:Boolean = true;				
				var tryX:int = x + vx;

				if (tryX < 0) tryX = 0;
				if (tryX + width > Game.Renderer.width) tryX = Game.Renderer.width - width;		
				
				collisionPoints = new Array(new Point(tryX + BUFFER, y + BUFFER), new Point(tryX + width - BUFFER, y + BUFFER), new Point(tryX + BUFFER, y + height - BUFFER), new Point(tryX + width - BUFFER, y + height - BUFFER));
				for (var i:int = 0; i < collisionPoints.length; i++ ) {
					var mapX:int = (int)(collisionPoints[i].x / Game.TILESIZE) % Game.map.length;
					var mapY:int = (int)(collisionPoints[i].y / Game.TILESIZE) % Game.map[0].length;
					if (!Game.map[mapX][mapY].passable) 
						pass = false;
				}
				
				if (pass) {
					x += vx;
					vx *= friction;
				}
				else {
					vx = 0;
				}
			}
			
			//Y Movement
			if (vy != 0) {
				var pass:Boolean = true;				
				var tryY:int = y + vy;

				if (tryY < 0) tryY = 0;
				if (tryY + height > Game.Renderer.height) tryY = Game.Renderer.height - height;				
				
				collisionPoints = new Array(new Point(x + BUFFER, tryY + BUFFER), new Point(x + width - BUFFER, tryY + BUFFER), new Point(x + BUFFER, tryY + height - BUFFER), new Point(x + width - BUFFER, tryY + height - BUFFER));
				for (var i:int = 0; i < collisionPoints.length; i++ ) {
					var mapX:int = (int)(collisionPoints[i].x / Game.TILESIZE) % Game.map.length;		//TODO: Solve this error, remove modulus
					var mapY:int = (int)(collisionPoints[i].y / Game.TILESIZE) % Game.map[0].length;
					if (!Game.map[mapX][mapY].passable) 
						pass = false;
				}
	
				if(pass){
					y += vy;
					vy *= friction;
				}
				else {
					vy = 0;
				}
			}	
			
			//Check bounds
			if (x < 0) x = 0;
			if (x + width > Game.Renderer.width) x = Game.Renderer.width - width;
			if (y < 0) y = 0;
			if (y + height > Game.Renderer.height) y = Game.Renderer.height - height;					
		}
		
		override public function render():void {
			//Position
			matrix = new Matrix();
			matrix.translate(-width / 2, -height / 2);
			matrix.rotate(angle + Math.PI/2);//Shift because of how it was drawn
			matrix.translate(width / 2, height / 2);
			matrix.translate(x, y);
			
			//Animation
			if (animationCount == 5) {
				animationIndex++;
				animationCount = 0;
				animationIndex = animationIndex % 4;
			}
			else{
				animationCount++;
			}
			if(Math.abs(vx) + Math.abs(vy) < 2)
				animationIndex = 1;
				
			animRect.x = animationIndex * Game.TILESIZE			
			
			if (animationIndex == 3) animRect.x = 1 * Game.TILESIZE;
			
			if (Game.state != Game.GAME)
				animRect.x = 1 * Game.TILESIZE;		
			
			var BMD:BitmapData = new BitmapData(Game.TILESIZE, Game.TILESIZE, true);
			BMD.copyPixels(bit.bitmapData, animRect, new Point(0, 0));
			var cutBit:Bitmap = new Bitmap(BMD);
			imageSprite = new Sprite();
			imageSprite.addChild(cutBit);
			Game.Renderer.draw(imageSprite, matrix);
		}
		
		public function moveUp():void {
			vy -= acceleration * Game.deltaTime;
		}
		
		public function moveDown():void {
			vy += acceleration * Game.deltaTime;
		}
		
		public function moveLeft():void {
			vx -= acceleration * Game.deltaTime;
		}
		
		public function moveRight():void {
			vx += acceleration * Game.deltaTime;
		}
		
	}

}