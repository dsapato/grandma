package Entities 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Zombie extends GameSprite {	
		[Embed(source = "../Resources/Images/zombie.png")]
		private var tex:Class;
		private var bit:Bitmap;
		
		[Embed(source = "../Resources/Images/zombie2.png")]
		private var tex2:Class;
		
		[Embed(source = "../Resources/Images/zombie3.png")]
		private var tex3:Class;		
		
		private var moveSpeed:int = 60;
		
		public var path:Vector.<MapTile>;
		private var goalX:int;
		private var goalY:int;
		private var matrix:Matrix;
		private var xDis:int;
		private var yDis:int;
		private var angle:Number;
		
		private var oneSec:int;
		
		private var collisionPoints:Vector.<Point>;
		private const BUFFER:int = 5;
		public var health:int;
		
		private var inPlay:Boolean = false;
		
		private var animationCount:int = 0;
		private var animationIndex:int = 0;
		private var animRect:Rectangle;	
		private var BMD:BitmapData;
		
		private const zeroPoint:Point = new Point(0, 0);
		
		public function Zombie(xx:int, yy:int, w:int, h:int, d:int=0) {
			super(xx, yy, w, h, d);
			goalX = x;
			goalY = y;
			vx = moveSpeed;
			vy = moveSpeed;
			
			if (Math.random() > .9) {	
				if (Math.random() > .5) {			//Zombie 2 (Strong)
					health = 3;
					bit = new tex2();
				}
				else {							//Zombie 3 (Fast)
					health = 1;
					moveSpeed = 100;
					bit = new tex3();
				}
			}
			else {								//Base Zombie
				health = 1;
				bit = new tex();
			}
			
			imageSprite = new Sprite();
			imageSprite.addChild(bit);
			matrix = new Matrix();
			animRect = new Rectangle(0, 0, Game.TILESIZE, Game.TILESIZE);
			BMD = new BitmapData(Game.TILESIZE, Game.TILESIZE, true);

			collisionPoints = new Vector.<Point>;
			collisionPoints.push(new Point(x, y));
			collisionPoints.push(new Point(x + width, y));
			collisionPoints.push(new Point(x, y + height));
			collisionPoints.push(new Point(x + width, y + height));

		}
		
		override public function update():void {
			if (getTimer() - oneSec > 1000) {
				oneSec = getTimer();
				var center:int = Game.TILESIZE / 2;
				path = AStar2.search(x + center, y + center, Game.grandma.x + center, Game.grandma.y + center); //Center-to-center
			}
			
			if (path == null) {
				goalX = Game.grandma.x;
				goalY = Game.grandma.y;
			}
			
			if (path != null && path.length != 0) {
				goalX = path[path.length - 1].x * Game.TILESIZE; 
				goalY = path[path.length - 1].y * Game.TILESIZE;
			}			
			
			xDis = goalX - x;
			yDis = goalY - y;
			
			if (xDis < -3) {
				dir = 2;
				x -= vx * Game.deltaTime;
			}
			else if (xDis > 3) {
				dir = 0;
				x += vx * Game.deltaTime;
			}			
			if (yDis < -3) {
				if (Math.abs(xDis) < Math.abs(yDis) + 1) dir = 1;
				y -= vy * Game.deltaTime;
			}
			else if (yDis > 3) {
				if (Math.abs(xDis) < Math.abs(yDis) + 1) dir = 3;
				y += vy * Game.deltaTime;
			}
		
			//Check bounds
			if (!inPlay && x > 0 && y > 0 && x + width > Game.Renderer.width && y + height > Game.Renderer.height)
				inPlay = true;
			
			if(inPlay){
				if (x < 0) x = 0;
				if (x + width > Game.Renderer.width) x = Game.Renderer.width - width;
				if (y < 0) y = 0;
				if (y + height > Game.Renderer.height) y = Game.Renderer.height - height;
			}
			
			vx = moveSpeed;
			vy = moveSpeed;			
			
			if (path != null && path.length != 0 && Math.abs(xDis) < 45 && Math.abs(yDis) < 45) path.splice(path.length - 1, 1);
		}
		
		override public function render():void {
			//Find Angle
			angle = Math.atan((goalY - y) / (goalX - x))
			if (goalX < x) angle += Math.PI //arctan only 180 degree range
			
			//Transform
			matrix.identity();
			matrix.translate(-width / 2, -height / 2);
			matrix.rotate(angle + Math.PI/2);//Shift because of how it was drawn
			matrix.translate(width / 2, height / 2);
			matrix.translate(x, y);
			
			//Animation
			if (animationCount == 4) {
				animationIndex++;
				animationCount = 0;
				animationIndex = animationIndex % 4;
			}
			else{
				animationCount++;
			}
			if(Math.abs(xDis) + Math.abs(yDis) < 6)
				animationIndex = 1;
				
			animRect.x = animationIndex * Game.TILESIZE
			if (animationIndex == 3) animRect.x = 1 * Game.TILESIZE;
			
			if (Game.state != Game.GAME)
				animRect.x = 1 * Game.TILESIZE;
			
			BMD.copyPixels(bit.bitmapData, animRect, zeroPoint);
			
			Game.Renderer.draw(BMD, matrix);
		}
	}
}