package Entities 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Marking extends GameSprite {
		public var life:int;
		public var type:String;
		public var remove:Boolean = false;
		private var matrix:Matrix;
		
		[Embed(source = "../Resources/Images/explosion.png")]
		private var explosionTex:Class;
		
		[Embed(source = "../Resources/Images/blood2.png")]
		private var bloodTex:Class;
		
		[Embed(source = "../Resources/Images/ground.png")]
		private var groundTex:Class;
		
		public function Marking(xx:Number, yy:Number, life:int, type:String) {
			if (type == "explosion") { xx - Game.TILESIZE / 2; yy - Game.TILESIZE / 2 }
			var width:int = Game.TILESIZE;
			var height:int = Game.TILESIZE;
			if (type == "ground"){ width = 116; height = 114;}
			super(xx, yy, width, height);
			this.life = life;
			this.type = type;
			imageSprite = new Sprite();
			var tempBit:Bitmap;
			matrix = new Matrix();
			if(type == "ground")matrix.rotate(Math.random() * Math.PI);
			matrix.translate(x, y);
			if (type == "explosion") tempBit = new explosionTex();
			else if (type == "blood") tempBit = new bloodTex();
			else tempBit = new groundTex();
			imageSprite.addChild(tempBit);
		}
		
		override public function update():void {
			if (type == "explosion" && getTimer() - life > 100) remove = true;
			else if (type == "blood" && getTimer() - life > 5000) remove = true;
		}
		
		override public function render():void {
			Game.Renderer.draw(imageSprite, matrix);					
		}
		
	}

}