package Entities 
{
	import flash.display.BitmapData;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class GameSprite{
		public var x:Number;
		public var y:Number;
		public var vx:Number;
		public var vy:Number;
		public var width:int;
		public var height:int;
		public var dir:int;
		
		protected var image:BitmapData;
		protected var imageSprite:Sprite;
		
		public function GameSprite(xx:int, yy:int, w:int, h:int, d:int = 0) {
			x = xx;
			y = yy;
			width = w;
			height = h;
			dir = d;
		}
		
		public function update():void {
			
		}
		
		public function render():void {
		
		}
		
	}

}