package Entities 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.errors.StackOverflowError;
	import flash.geom.Matrix;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Gun extends GameSprite {
		public var matrix:Matrix;
		public var type:int;
		
		[Embed(source="../Resources/Images/Guns/pistolTop.png")]
		private var pistolTopTex:Class;
		private var pistolBitmap:Bitmap;
		
		[Embed(source = "../Resources/Images/Guns/machinegunTop.png")]
		private var MGTopTex:Class;
		private var MGBitmap:Bitmap;
		
		[Embed(source = "../Resources/Images/Guns/shotgunTop.png")]
		private var shotTopTex:Class;
		private var shotBitmap:Bitmap;
		
		[Embed(source = "../Resources/Images/Guns/doubleMachineGunTop.png")]
		private var DMGTopTex:Class;
		private var DMGBitmap:Bitmap;
		
		[Embed(source = "../Resources/Images/Guns/sniperTop.png")]
		private var sniperTopTex:Class;
		private var sniperBitmap:Bitmap;
		
		[Embed(source = "../Resources/Images/Guns/rocketLauncherTop.png")]
		private var rocketTex:Class;
		private var rocketBitmap:Bitmap;
		
		[Embed(source = "../Resources/Images/Guns/railgunTop.png")]
		private var railTex:Class;
		private var railBitmap:Bitmap;
		
		[Embed(source = "../Resources/Images/Guns/pepperSprayTop.png")]
		private var pepTex:Class;
		private var pepBitmap:Bitmap;
		
		
		
		public function Gun(xx:int = 0, yy:int = 0, w:int = Game.TILESIZE, h:int = Game.TILESIZE, d:int=0) {
			super(xx, yy, w, h, d);

			imageSprite = new Sprite();
			type = 0;
			pistolBitmap = new pistolTopTex();
			MGBitmap = new MGTopTex();
			shotBitmap = new shotTopTex();
			DMGBitmap = new DMGTopTex();
			sniperBitmap = new sniperTopTex();
			rocketBitmap = new rocketTex();
			railBitmap = new railTex();
			pepBitmap = new pepTex();
			imageSprite.addChild(pistolBitmap);
			matrix = new Matrix();
		}
		
		override public function update():void {

		}
		
		override public function render():void {
			matrix = new Matrix();
			matrix.translate(-width / 2, -height / 2);
			matrix.rotate(Game.grandma.angle + Math.PI/2);
			matrix.translate(width / 2, height / 2);
			var xShift:int = -30*Math.sin(Game.grandma.angle) + -5*Math.cos(Game.grandma.angle);
			var yShift:int = 30*Math.cos(Game.grandma.angle) + -5*Math.sin(Game.grandma.angle);
			matrix.translate(Game.grandma.x + xShift, Game.grandma.y + yShift);

			imageSprite = new Sprite();
			if 		(type == 0)	imageSprite.addChild(pistolBitmap);
			else if (type == 1) imageSprite.addChild(MGBitmap);
			else if (type == 2) imageSprite.addChild(shotBitmap);
			else if (type == 3) imageSprite.addChild(DMGBitmap);
			else if (type == 4) imageSprite.addChild(sniperBitmap);
			else if (type == 5) imageSprite.addChild(rocketBitmap);
			else if (type == 6) imageSprite.addChild(railBitmap);
			else 				imageSprite.addChild(pepBitmap);
			Game.Renderer.draw(imageSprite, matrix);					
		}
	}
}