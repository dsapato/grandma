package Entities 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.errors.StackOverflowError;
	import flash.geom.Matrix;
	import flash.net.NetStreamMulticastInfo;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Projectile extends GameSprite {
		[Embed(source="../Resources/Images/bullet.png")]
		private var bulletTex:Class;
		
		[Embed(source = "../Resources/Images/bigBullet.png")]
		private var bigBulletTex:Class;
		
		[Embed(source = "../Resources/Images/rocket.png")]
		private var rocketTex:Class;
		
		[Embed(source = "../Resources/Images/laser.png")]
		private var laserTex:Class;
		
		[Embed(source = "../Resources/Images/pepper.png")]
		private var pepTex:Class;
		
		public var type:String;
		private var speed:int = 300;
		public var age:int;
		public var angleRads:Number;
		
		public function Projectile(xx:int, yy:int, w:int, h:int, a:int, speed:int = 300, angRads:Number = 0, type:String = "bullet") {
			super(xx, yy, w, h);
			age = a;
			this.speed = speed;
			this.type = type;
			angleRads = angRads;
			imageSprite = new Sprite();
			var bit:Bitmap;
			if (type == "big bullet") bit = new bigBulletTex();
			else if (type == "rocket") bit = new rocketTex();
			else if (type == "laser") bit = new laserTex();
			else if (type == "pepper") bit = new pepTex();
			else bit = new bulletTex();
			imageSprite.addChild(bit);
		}
		
		override public function update():void {
			if (type != "laser"){
				x += speed * Game.deltaTime * Math.cos(angleRads);
				y += speed * Game.deltaTime * Math.sin(angleRads);
				
				if (x < 0) { x = 0; age = -10000; }
				if (x + width > Game.Renderer.width) { x = Game.Renderer.width - width; age = -10000};
				if (y < 0) { y = 0; age = -10000;}
				if (y + height > Game.Renderer.height) { y = Game.Renderer.height - height; age = -10000; }		
				if (type != "pepper" && !Game.map[(int)(x / Game.TILESIZE)][(int)(y / Game.TILESIZE)].passable)
						this.age = -10000;
			}
		}
		
		override public function render():void {
			var matrix:Matrix = new Matrix();
			matrix.translate(-width, -height);
			matrix.rotate(angleRads + Math.PI / 2);
			matrix.translate(width, height);
			matrix.translate(x - width, y - height);
	
 
			Game.Renderer.draw(imageSprite, matrix);					
		}
		
	}

}