package Entities 
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class Obstacle extends GameSprite {
		[Embed(source = "../Resources/Images/tree.png")]
		private var treeTex:Class;
		
		[Embed(source = "../Resources/Images/tree2.png")]
		private var tree2Tex:Class;
		
		[Embed(source = "../Resources/Images/rock.png")]
		private var rockTex:Class;
		
		private var type:String;
		
		public function Obstacle(xx:int, yy:int, w:int, h:int, type:String = "tree") {
			super(xx, yy, w, h);
			this.type = type;
			imageSprite = new Sprite();
			var tree_bitmap:Bitmap;
			if(Math.random() < .4)
				tree_bitmap= new treeTex();
			else{
				if(Math.random() < .6)
					tree_bitmap = new tree2Tex();
				else 
					tree_bitmap = new rockTex();
			}
			imageSprite.addChild(tree_bitmap);
		}
		
		override public function update():void {
			
		}
		
		override public function render():void {
			var matrix:Matrix = new Matrix();
			matrix.translate(x, y);
			Game.Renderer.draw(imageSprite, matrix);
		}
		
	}

}