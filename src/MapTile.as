package  
{
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class MapTile {
		public var x:int;
		public var y:int;
		public var passable:Boolean;
		public var f:int = 0;
		public var g:int = 0;
		public var h:int = 0;
		public var parent:MapTile;
		public var visited:Boolean;
		public var closed:Boolean;
		
		public function MapTile(x:int, y:int, pass:Boolean) {
			this.x = x;
			this.y = y;
			this.passable = true;
		}
		
	}

}