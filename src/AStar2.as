package  
{
	/**
	 * ...
	 * @author Danny Sapato
	 */
	public class AStar2 {
		private static var map:Vector.<Vector.<MapTile>>;
		
		public static function search(startX:Number, startY:Number, goalX:Number, goalY:Number):Vector.<MapTile> {
			if (startX < 0) startX *= -1;
			if (startY < 0 ) startY *= -1;
			while (startX >= 640) startX -= Game.TILESIZE;
			while (startY >= 480) startY -= Game.TILESIZE;
			map = Game.map;
	
			for (var i:int = 0; i < map.length; i ++) {
				for (var j:int = 0; j < map[0].length; j ++ ) {
					map[i][j].visited = false;
					map[i][j].closed = false;
				}
			}			
			
			var open:Vector.<MapTile> = new Vector.<MapTile>;
			var startNode:MapTile = new MapTile((int)(startX / Game.TILESIZE), (int)(startY / Game.TILESIZE), map[(int)(startX / Game.TILESIZE)][(int)(startY / Game.TILESIZE)].passable);
			var goalNode:MapTile = new MapTile((int)(goalX / Game.TILESIZE),(int)(goalY / Game.TILESIZE), map[(int)(goalX / Game.TILESIZE)][(int)(goalY / Game.TILESIZE)].passable);
			open.push(startNode);
			startNode.visited = true;
			
			if(startNode.passable && goalNode.passable){
				while (open.length > 0) {
					//Select best node so far
					var lowIndex:int = 0;
					for (var i:int = 0; i < open.length; i++ ) {
						if (open[i].f < open[lowIndex].f)
							lowIndex = i;
					}
					var currentNode:MapTile = open[lowIndex];
					
					//End Case, Path Found, return
					if (currentNode.x == goalNode.x && currentNode.y == goalNode.y) {
						var curr:MapTile = currentNode;
						var ret:Vector.<MapTile> = new Vector.<MapTile>;
						while (curr.parent != null) {
							ret.push(curr);
							curr = curr.parent;
						}
						return ret;					
					}
					
					//Normal Case, Current Node to closed, Add Neighbors to open
					open.splice(lowIndex, 1);
					currentNode.closed = true;
					var neighbors:Vector.<MapTile> = findNeighbors(currentNode);
					for (var i:int = 0; i < neighbors.length; i++ ) {
						var neighbor:MapTile = neighbors[i];
						if (neighbor.closed || !neighbor.passable) continue; //Skip because seen or unpassable
						
						var gScore:int = currentNode.g + 1;
						var gScoreIsBest:Boolean = false;
						
						if (!neighbor.visited) {//first time found
							gScoreIsBest = true;
							neighbor.h = findH(neighbor, goalNode);
							open.push(neighbor);
							neighbor.visited = true;
						}
						
						else if (gScore < neighbor.g) {//Better gScore than before
							gScoreIsBest = true;
						}
						
						if (gScoreIsBest) {//store info
							neighbor.parent = currentNode;
							neighbor.g = gScore;
							neighbor.f = neighbor.g + neighbor.h;
						}
						
					}
				}
				trace("No path found");
				return null;
			}
			trace("Start/Goal Not Passable");
			return null;
		}
		
		private static function findNeighbors(curNode:MapTile):Vector.<MapTile> {
			var ret:Vector.<MapTile> = new Vector.<MapTile>;
			var x:int = curNode.x;
			var y:int = curNode.y;
			
			if (x - 1 >= 0) ret.push(map[x - 1][y]);
			if (x + 1 < map.length) ret.push(map[x + 1][y]);
			if (y - 1 >= 0) ret.push(map[x][y - 1]); 
			if (y + 1 < map[0].length) ret.push(map[x][y + 1]);
	
			return ret;
		}
		
		private static function findH(curNode:MapTile, goalNode:MapTile):int {
			return Math.abs(curNode.x - goalNode.x) + Math.abs(curNode.y - goalNode.y);
		}
	}

}